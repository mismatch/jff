import * as grid from "./modules/pirates-grid.js";
import * as game from "./modules/pirates-game.js";

const columns = ['a','b','c','d','e','f','g'];
const rows = [1,2,3,4,5,6,7];

// event listeners
document.getElementById('random-button').addEventListener('click', game.randomSquare);
grid.squaresContainer.addEventListener('click', game.flipSquare);

//generate the board
grid.generateSquaresList(rows,columns);
grid.generateSquaresHtml(grid.squaresList);
grid.generateTopHeadingsHtml(columns);
grid.generateLeftHeadingsHtml(rows);

// randomise the squares
game.randomiseSquaresAvailable(grid.squaresList);
