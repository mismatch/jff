export let squaresContainer = document.getElementById('squares-container');
export let squaresList = [];

// create a list of squares a1, a2, ..., c4, ...
export function generateSquaresList (rows, columns) {
    for (const row of rows) { 
        for (const column of columns){
            squaresList.push(column + row);
        }
    }
    return squaresList;
}

// write divs that represent the individual squares to the dom dynamically
export function generateSquaresHtml (squaresList) {
    for(const square of squaresList){
        let htmlSquareText = (`<div class="square" data-location="${ square }">
    <div class="front-face"></div>
    <div class="back-face"></div>
</div>

`)
        squaresContainer.innerHTML += htmlSquareText;
    }
}

export function generateLeftHeadingsHtml (headingsList) {
    let htmlContainer = document.getElementById('left-bar');
    for (const heading of headingsList){
        let htmlHeadingText = (`<p class="left-label">${ heading }</p>`);
        htmlContainer.innerHTML += htmlHeadingText;
    }
}

export function generateTopHeadingsHtml (headingsList) {
    let htmlContainer = document.getElementById('top-bar');
    for (const heading of headingsList){
        let htmlHeadingText = (`<p class="top-label">${ heading.toUpperCase() }</p>`);
        htmlContainer.innerHTML += htmlHeadingText;
    }
}
