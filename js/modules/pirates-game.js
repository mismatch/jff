import { randomise } from "./utils.js";

let squaresAvailable = [];
let squaresCalled = [];
// needs function delegation to select the correct nodes for the flip
// since the square divs are created dynamically
// it receives an event object from the click event
export function flipSquare (e) {
    let selectedFlipSquare = e.target.parentNode;
    let selectedFlipLocation = selectedFlipSquare.dataset.location;
    // if any div other than a square is clicked 
    // (e.g. the squares-container) then exit the function
    if(!selectedFlipSquare.matches('.square')) return;
    selectedFlipSquare.classList.toggle('flip'); 
    // if the square is not yet called make a move
    // otherwise undo the move 
    !squaresCalled.includes(selectedFlipLocation) ? makeMove(selectedFlipLocation) : removeMove(selectedFlipLocation);
    printLastCalled(squaresCalled[0]);
    randomise(squaresAvailable);
    highlightLastCalled();
}

function makeMove (square) {
    squaresCalled.unshift(square);
    return squaresAvailable = squaresAvailable.filter(p => p !== square);
}

function removeMove (square) {
    squaresAvailable.unshift(square)
    return squaresCalled = squaresCalled.filter(p =>p !== square);
}

export function randomSquare () {
    let square = squaresAvailable.pop();
    let squareLocation = document.querySelector(`[data-location="${square}"]`);
    squareLocation.classList.toggle('flip');
    squaresCalled.unshift(square);
    printLastCalled(squaresCalled[0]);
    highlightLastCalled();
}

function printLastCalled (square) {
    if(square==undefined) return;
    let lastSquareText = document.getElementById('last-square-info');
    lastSquareText.innerHTML = `last square: ${square.toUpperCase()}`;
}

function highlightLastCalled () {
    removeHighlights();
    let lastSquareLocation = document.querySelector(`[data-location="${squaresCalled[0]}"]`);
    if (lastSquareLocation == null) return;
    lastSquareLocation.children[1].classList.add('last');
}

function removeHighlights () {
    let allSquares = document.querySelectorAll('.square');
    allSquares.forEach(square => square.children[1].classList.remove('last'));
}

export function randomiseSquaresAvailable (list) {
    return squaresAvailable = randomise(list);
}

// alert when trying to close window
window.onbeforeunload = function() {
    return 'Are you sure you want to leave this game?';
};
