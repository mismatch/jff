// randomise an input array (items) using fisher-yates shuffle
export function randomise (items) {
    for (let i = items.length -1; i > 0; i--) {
        let j = Math.floor(Math.random() * i);
        let k = items[i];
        items[i] = items[j];
        items[j] = k;
    } 
    return items;
}
